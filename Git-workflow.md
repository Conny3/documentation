## Introduction

These instructions are for using [git](https://git-scm.com) to manage Google Earth Engine (GEE) scripts in Gitlab.

The GEE documentation makes very brief and obscure mention of the git functionality under the [script manager tab](https://developers.google.com/earth-engine/playground#script-manager-scripts-tab) section. In this guide, we will work through setting up access to GEE via git, and using that to pull and push code between GEE and Gitlab.

## Setting up Git access to GEE

1. After logging into Google Earth engine, in a new tab, navigate to https://earthengine.googlesource.com. Make sure your Google account shows up in the bar at the top of the page. Otherwise click the 'Sign In' icon and log in again. Now click the "Generate Password" link:

![Screen shot showing earthengine.googlesource.com](Git-workflow-images/setup0.png)

2. The site will prompt you to select a Google for which you will be generating the Git password, select the same account you use for Google Earth Engine:

![Screen shot showing account selection](Git-workflow-images/setup1.png)

3. Now permissions for the password to access your googlesource.com git repositories:

![Screen shot showing permissions grant](Git-workflow-images/setup2.png)

4. Finally, you'll receive instructions to create a git http cookie credential on your computer, follow these instructions in the screenshot below. Note: install Git first (Git for Windows can be installed without admin rights). If succeeded, your repository should be in the (long!) list of repos on earthengine.googlesource.com. Check this.

![Screen shot showing git http cookie](Git-workflow-images/setup3.png)

## Creating a new repository in GEE

In the GEE scripts manager tab, click the New button to create a new _repository_ (repo):

![Screen shot showing adding a new repo in GEE](Git-workflow-images/0.png)

Now in the popup, you can choose a name for the repo. We'll be using a script to generate a Sentinel-2 cloud-free mosaic for Northland Region written by @rogierwesterhoff, so we'll call it `nrc_s2_mosaic`:

![Screen shot showing repo name entry](Git-workflow-images/1.png)

Now a new entry appears under the "Owner" section of the Scripts manager tab:

![Screen shot showing new repo in scripts manager tab](Git-workflow-images/2.png)

## Add some code to the new repo

To create a new file in the `nrc_s2_mosaic` repo, click the New button and select _file_:

![Screen shot showing adding a new file to the repo](Git-workflow-images/3.png)

In the dialog that appears, select the repo in the drop-down box:

![Screen shot showing create file 1/2](Git-workflow-images/4.png)

Then choose a name for the file, and give it a description for the commit log:

![Screen shot showing create file 2/2](Git-workflow-images/5.png)

Now click on the new file in the scripts manager tab to open it in the code editor:

![Screen shot showing opening new repo file in code editor](Git-workflow-images/6.png)

Finally, we enter some code, and save the file:

![Screen shot showing saving file 1/2](Git-workflow-images/7.png)

...entering a description for the change we made:

![Screen shot showing saving file 2/2](Git-workflow-images/8.png)

Clicking on the history icon next to the file in the scripts manager tab shows the change history:

![Screen shot showing change history ](Git-workflow-images/9.png)

## Getting the git repo URL

Click on the "Configure" icon next to the new repo in the scripts manager tab:

![Screen shot showing repo configure icon](Git-workflow-images/10.png)

Now the dialog shows the git repository URL (copy this to your clipboard). Here you can optionally:
- Grant read access to the public
- Grant read or write access to specific Google accounts

![Screen shot showing share repo dialog](Git-workflow-images/11.png)

## Cloning the repo with git

Here I providde an example of a git clone of the repository to my local Linux machine (using the URL from the section above):

```
$ git clone https://earthengine.googlesource.com/users/simeonmiteff_xerra/nrc_s2_mosaic
Cloning into 'nrc_s2_mosaic'...
remote: Total 6 (delta 0), reused 6 (delta 0)
Receiving objects: 100% (6/6), done.
$ cd nrc_s2_mosaic/
$ ls
main.js
```
(note: use 'dir' instead of 'ls' if you're working on Windows)

By default googlesource.com will be the remote named "origin":
```
$ git remote -v
origin	https://earthengine.googlesource.com/users/simeonmiteff_xerra/nrc_s2_mosaic (fetch)
origin	https://earthengine.googlesource.com/users/simeonmiteff_xerra/nrc_s2_mosaic (push)
```

Let's rename that to "gee" to avoid confusion later on:
```
$ git remote rename origin gee

$ git remote -v
gee	https://earthengine.googlesource.com/users/simeonmiteff_xerra/nrc_s2_mosaic (fetch)
```

## Creating a new gitlab repo

Log into gitlab, and click the "New project" button, either in your personal group, or in the EnviroSatTools group:

![Screen shot showing EnivroSatTools group](Git-workflow-images/12.png)

Entering `nrc_s2_mosaic` in the Project name field is sufficient in my example, then I click "Create project".

## Adding and pushing to gitlab as a git remote

Now we add the new (empty) Gitlab repository as a remote named "gitlab"
```
$ git remote add gitlab git@gitlab.com:envirosattools/nrc_s2_mosaic.git

$ git remote -v
gee	https://earthengine.googlesource.com/users/simeonmiteff_xerra/nrc_s2_mosaic (fetch)
gee	https://earthengine.googlesource.com/users/simeonmiteff_xerra/nrc_s2_mosaic (push)
gitlab	git@gitlab.com:envirosattools/nrc_s2_mosaic.git (fetch)
gitlab	git@gitlab.com:envirosattools/nrc_s2_mosaic.git (push)
```

Finally, we push the code we cloned from googlesource.com to gitlab:
```
$ git push gitlab master
Enumerating objects: 6, done.
Counting objects: 100% (6/6), done.
Delta compression using up to 8 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (6/6), 2.13 KiB | 2.13 MiB/s, done.
Total 6 (delta 0), reused 6 (delta 0), pack-reused 0
To gitlab.com:envirosattools/nrc_s2_mosaic.git
 * [new branch]      master -> master
```

## Forking a project and pushing it to GEE

Now image a different user wants to make their own variant of the project above...

We click on the "Fork" button:

![Screen shot showing repo fork button](Git-workflow-images/16.png)

The next screen prompts for selection of a namespace. For the purpose of this demonstration, I will use my personal namespace (https://gitlab.com/simeonmiteff). After selecting that, the following screen shows the result:

![Screen shot showing repo fork button](Git-workflow-images/17.png)

Next I click the "Clone" button and copy the SSH URL, and clone the repository (using a different destination, since I already have a local verson of `nrc_s2_mosaic`):

```
$ git clone git@gitlab.com:simeonmiteff/nrc_s2_mosaic.git arc_s2_mosaic
Cloning into 'arc_s2_mosaic'...
remote: Enumerating objects: 6, done.
remote: Counting objects: 100% (6/6), done.
remote: Compressing objects: 100% (3/3), done.
remote: Total 6 (delta 0), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (6/6), done.
```

Now I repeat the steps to create a new _repository_ in GEE described above, except I call it `arc_s2_mosaic` this time, and rename the remote from `origin` to `gitlab`, adding `gee` using the URL from GEE (i.e., the reverse of what I did before):

```
$ git remote rename origin gitlab
$ git remote add gee https://earthengine.googlesource.com/users/simeonmiteff_xerra/arc_s2_mosaic
$ git remote -v
gee	https://earthengine.googlesource.com/users/simeonmiteff_xerra/arc_s2_mosaic (fetch)
gee	https://earthengine.googlesource.com/users/simeonmiteff_xerra/arc_s2_mosaic (push)
gitlab	git@gitlab.com:simeonmiteff/nrc_s2_mosaic.git (fetch)
gitlab	git@gitlab.com:simeonmiteff/nrc_s2_mosaic.git (push)
```

Finally, we can push the code from the forked gitlab repository to our new (empty) GEE project:

```
$ git push gee master
Enumerating objects: 6, done.
Counting objects: 100% (6/6), done.
Delta compression using up to 8 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (6/6), 2.13 KiB | 2.13 MiB/s, done.
Total 6 (delta 0), reused 6 (delta 0), pack-reused 0
remote: Waiting for 1/2
remote: Approximate storage used: 2.1KiB/30.0MiB
To https://earthengine.googlesource.com/users/simeonmiteff_xerra/arc_s2_mosaic
 * [new branch]      master -> master
```

`main.js` now shows up in the script manager tab in GEE:

![Screen shot showing file from forked repo appear in GEE](Git-workflow-images/18.png)

## Review: what have we achieved?

- We've used git to pull and push code between a local repository two remote repositories, one at GEE and one at gitlab.
- We forked the gitlab repository and used the same mechanism as above to pull and push code to a new GEE repository.

Overkill? Indeed, we could have cut-and-pasted the contents of `main.js` between two projects in GEE. The benefits of this workflow will become apparent as we begin making changes to both projects, and wish to propagate those changes in both directions, in a controlled manner.
