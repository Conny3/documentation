# Documentation

## Workshop presentations

- [Workshop 1: Overall presentation](/RC_Satelllite_Tools_Workshop_1_Overall_presentation.pdf) ([PDF download](https://gitlab.com/envirosattools/documentation/-/raw/master/RC_Satelllite_Tools_Workshop_1_Overall_presentation.pdf?inline=false))
- [Workshop 1: Presentation University Auckland](/RC_Satelllite_Tools_Workshop 1 Uni Auckland.pdf) ([PDF download](https://gitlab.com/envirosattools/documentation/-/raw/master/RC_Satellite_Tools_Workshop_1_Uni_Auckland.pdf?inline=false))
- [Workshop 1: Presentation Indufor](/RC_Satellite_Tools_Workshop_1_Indufor_Envirolink.pdf) ([PDF download](https://gitlab.com/envirosattools/documentation/-/raw/master/RC_Satellite_Tools_Workshop_1_Indufor_Envirolink.pdf?inline=false))

- [Workshop 2: Intro presentation](/Workshop_2_Intro_presentation.pdf) ([PDF download](https://gitlab.com/envirosattools/documentation/-/raw/master/Workshop_2_Intro_presentation.pdf?inline=false))
- [Workshop 2: Example Apps](/Workshop_2_EE_Apps_Indufor.pdf) ([PDF download](https://gitlab.com/envirosattools/documentation/-/raw/master/Workshop_2_EE_Apps_Indufor.pdf?inline=false))
- [Workshop 2: Examples from councils: Northland](/Workshop_2_Example_from_NRC.pdf) ([PDF download](https://gitlab.com/envirosattools/documentation/-/raw/master/Workshop_2_Example_from_NRC.pdf?inline=false))
- [Workshop 2: Examples from councils: Waikato](/Workshop_2_Example_from_WRC.pdf) ([PDF download](https://gitlab.com/envirosattools/documentation/-/raw/master/Workshop_2_Example_from_WRC.pdf?inline=false))
- [Workshop 2: Examples: LiDAR ](/Workshop_2_Mauku_LiDAR_Indufor.pdf) ([PDF download](https://gitlab.com/envirosattools/documentation/-/raw/master/Workshop_2_Mauku_LiDAR_Indufor.pdf?inline=false))
- [Workshop 2: Hands-on exercises](/Workshop_2_Hands-on_exercises.pdf) ([PDF download](https://gitlab.com/envirosattools/documentation/-/raw/master/Workshop_2_Hands-on_exercises.pdf?inline=false))

## Workshop recordings
(Workshop 1 recording not available)

Workshop 2 recordings are available here:
- [Workshop 2: Intro presentation](https://youtu.be/42ZbYLMF8wc)
- [Workshop 2: Example Apps](https://youtu.be/WoUVuB2oNxw)
- [Workshop 2: Examples from councils: Northland](https://youtu.be/pdnk00AvCE8)
- [Workshop 2: Examples from councils: Waikato](https://youtu.be/X9BvlGzSO7E) 
- [Workshop 2: Examples: LiDAR](https://youtu.be/gsk6F41PPco)
- [Workshop 2: Hands-on exercises](https://youtu.be/MHVW18h4PMg) 
- [Workshop 2: Collaborative workspace Details: GitLab](https://youtu.be/BMwhwtbssCc)
- [Workshop 2: Discussion](https://youtu.be/mQMIGk0CNsA)

## Workshop tutorials
- [Tutorial 1](/Tutorial1_Discover_GEE_tutorial_v0_8.pdf) ([PDF download](https://gitlab.com/envirosattools/documentation/-/raw/master/Tutorial1_Discover_GEE_tutorial_v0_8.pdf?inline=false))
- [Tutorial 2](/Tutorial2_Hansen_Global_Forest_Change_v0_5.pdf) ([PDF download](https://gitlab.com/envirosattools/documentation/-/raw/master/Tutorial2_Hansen_Global_Forest_Change_v0_5.pdf?inline=false))
 